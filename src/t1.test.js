describe("test", () => {
  it("tests global", async () => {
    const t = jest.fn();
    global.t = t;
    await new Promise((res) => setTimeout(res, 100));
    expect(global.t).toBe(t);
  });
});
