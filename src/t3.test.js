describe("test", () => {
  it("tests global", () => {
    const t = jest.fn();
    global.t = t;
    expect(global.t).toBe(t);
  });
});
